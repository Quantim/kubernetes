# Příkazy ke kurzu Kubernetes

Dokument slouží jako pomůcka při výuce a jako reference pro osvěžení znalostí

## Instalace dockeru

Instalujeme docker engine z oficiáního docker repozitáře podle instrukcí na [https://docs.docker.com/engine/install/ubuntu/](https://docs.docker.com/engine/install/ubuntu/).

Odebrání balíčků u distribuce
```
for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc; do sudo apt-get remove $pkg; done
```
Nainstalování závislostí a přidání repozitáře
```
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```
Nainstalování dockeru
```
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```
A otestování funkčnosti
```
sudo docker run hello-world
```

## Instalace minikube
Minikube používáme pro tvorbu lokálního clusteru instalaci provádíme dle instrukcí na [https://minikube.sigs.k8s.io/docs/start/?arch=%2Flinux%2Fx86-64%2Fstable%2Fbinary+download](https://minikube.sigs.k8s.io/docs/start/?arch=%2Flinux%2Fx86-64%2Fstable%2Fbinary+download)

Použijeme hotové binární soubory
```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube && rm minikube-linux-amd64
```
A minikube spustíme
```
minikube start
```
A ověříme, zda nám cluter běží a vidíme pody
```
minikube kubectl -- get po -A
```
juju error
```
sudo sysctl fs.protected_regular=0
```

```
minikube addons enable metrics-server
minikube dashboard
```

## Instalace kubectl

```
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```
## Nasazení aplikace
```
kubectl create deployment hello-minikube --image=kicbase/echo-server:1.0
kubectl expose deployment hello-minikube --type=NodePort --port=8080
minikube service hello-minikube
```
### Co se stalo
```
kubectl create deployment hello-minikube --image=kicbase/echo-server:1.0 -o yaml --dry-run=client
```
```
kubectl expose deployment hello-minikube --type=NodePort --port=8080 -o yaml --dry-run=client
```

kubectl logs -f hello-minikube-7d48979fd6-nvhg8 -c echo-server

kubectl scale -n default deployment hello-minikube --replicas=1

### Změny v deploymentu
```
kubectl edit deployment hello-minikube
```


kubectl create configmap hello-minikube -o yaml --dry-run=client >configmap.yaml


```
apiVersion: v1
kind: ConfigMap
metadata:
  creationTimestamp: null
  name: hello-minikube
data:
  NGINX_PORT: "8080"
  default.conf.template: |
    server {
      listen       ${NGINX_PORT};
    }
```

```
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: hello-minikube
  name: hello-minikube
spec:
  replicas: 1
  selector:
    matchLabels:
      app: hello-minikube
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: hello-minikube
    spec:
      containers:
      - env:
        - name: NGINX_PORT
          valueFrom:
            configMapKeyRef:
              key: NGINX_PORT
              name: hello-minikube
        image: nginx:latest
        imagePullPolicy: IfNotPresent
        name: echo-server
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        volumeMounts:
        - mountPath: /etc/nginx/templates/default.conf.template
          name: config
          readOnly: true
          subPath: default.conf.template
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
      volumes:
      - configMap:
          defaultMode: 420
          items:
          - key: default.conf.template
            path: default.conf.template
          name: hello-minikube
        name: config

```

### HPA
```
kubectl autoscale deployment hello-minikube  --min=1 --max=5
kubectl get hpa
```
```
kubectl run -i --tty load-generator --rm --image=ubuntu --restart=Never -- /bin/bash
```
Uvnitr kontejneru
```
apt update
apt install apache2-utils
ab -n 10000000 -c 5000 http://hello-minikube:8080/
```

upravit deployment

```
kubectl edit deployment hello-minikube
```

pridat misto `resources: {}`
```
        resources:
          requests:
             cpu: 100m
```

### Helm
```
curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
```

```
helm create hello-minikube
```
```
helm template hello-minikube .
helm template --debug hello-minikube .
```
uklid
```
kubectl delete service hello-minikube
kubectl delete deployment hello-minikube
```
```
helm install hello-minikube .
```
